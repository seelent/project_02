# UNITY-Dark_Pantheon

Dark Pantheon
DRAFT #1
Team
	Members
1.	Michał Jędrusiński


Responsibilities
1.	One man army

Project
Project Description
1.	Perspective: TPP 
2.	Graphics: 3D low-poly
3.	Type: action-rpg, souls-like
4.	Setting: semi-medieval

The game tells about a bounty hunter acting under the influence of one of the guilds. 

Project Components

1.	One level – open, explorable, backtracking

2.	Gameplay mechanics:
  a.	Player movement
    - idle
    - running
    - sprinting

  b.	Player actions
    - rolling (invulnerable)
    - targeting (lock-on)
    - dealing damage/attacking (light attack, combo attack, heavy attack)
    - consuming stamina by attacking
    - healing
    - taking damage
    - dying after taking some amount of damage

  c.	UI
    - health bar
    - stamina bar (regenerates itself after some time)
    - quick slots
    - main menu

  d.	Enemy A.I.
    - idle
    - targeting player
    - dealing damage/attacking player
    - taking damage
    - dying after taking some amount of damage

  e.	Interactive items
    - weapons (deals damage)
    - health potion (regenerates health)

  f.   Audio
    - footsteps (per animation)
    - sword slash
    - ambient sounds
    - combat music

3.	Types of enemies:
  a.	Enemy #1 –Soldier (low hp)
  b.	Enemy #2 – Knight (medium hp)
  c.	Enemy #3 – Boss (high hp)

4.	Duration of the project:
  ~ 5 min.
