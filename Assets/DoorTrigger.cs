﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public GameObject door;
    [SerializeField]
    private Animator myAnimationController;
    void OnTriggerEnter(Collider col)
    {   
        if (col.gameObject.tag == "Player")
        {
            myAnimationController.SetBool("playerPressure", true);
            print ("done");
            Destroy(door);
        }
         
    }

    void OnTriggerExit(Collider col)
    {   
        if (col.gameObject.tag == "Player")
        {
            myAnimationController.SetBool("playerPressure", false);
            print ("done");
            Destroy(door);
        }
         
    }
}
