﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace seelent
{
    public class ExpPointsBar : MonoBehaviour
    {
        public Text expPointsText;

        public void SetExpPointsText(int expPoints)
        {
            expPointsText.text = expPoints.ToString();
        }
    }
}