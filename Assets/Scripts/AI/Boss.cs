﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public GameObject Prefab;
    public Transform Spawnpoint;

    public GameObject Goal;
    [SerializeField]
    private Animator myAnimationController;
    private void OnDisable() 
    {
        myAnimationController.SetBool("open", true);
        Destroy(Prefab);
        Instantiate(Goal, Spawnpoint.position, Spawnpoint.rotation);
    }
}
