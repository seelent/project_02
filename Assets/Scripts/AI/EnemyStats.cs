﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace seelent
{
    public class EnemyStats : CharacterStats
    {
        EnemyAnimatorManager enemyAnimatorManager;

        public UIEnemyHealthBar enemyHealthBar;
        public int expAwardedOnDeath = 50;

        private void Awake()
        {
            enemyAnimatorManager = GetComponentInChildren<EnemyAnimatorManager>();
        }

        void Start()
        {
            maxHealth = SetMaxHealthFromHealthLevel();
            currentHealth = maxHealth;
            enemyHealthBar.SetMaxHealth(maxHealth);
        }

        private int SetMaxHealthFromHealthLevel()
        {
            maxHealth = healthLevel * 10;
            return maxHealth;
        }

        public void TakeDamageNoAnimation(int damage)
        {
            currentHealth = currentHealth - damage;

            if (currentHealth <= 0)
            {
                currentHealth = 0;
                isDead = true;

            }
        }
        public void TakeDamage(int damage)
        {
            if(isDead)
                return;

            currentHealth = currentHealth - damage;

            enemyHealthBar.SetHealth(currentHealth);

            enemyAnimatorManager.PlayTargetAnimation("Damage_01", true);

            if (currentHealth <= 0)
            {
                HandleDeath();
            }
        }

         IEnumerator delayactiv()
        {
            yield return new WaitForSeconds(5);
            gameObject.SetActive(false);
        }

        private void HandleDeath()
        {
            currentHealth = 0;
            enemyAnimatorManager.PlayTargetAnimation("Dead_01", true);
            isDead = true;
            GetComponent<EnemyManager>().enabled = false;
            StartCoroutine (delayactiv());
            //Scan for every player in the scene, award them exp
            PlayerStats playerStats = FindObjectOfType<PlayerStats>();
            ExpPointsBar expPointsBar = FindObjectOfType<ExpPointsBar>();

            if(playerStats != null)
            {
                playerStats.AddExp(expAwardedOnDeath);

                if(expPointsBar != null)
            {
                expPointsBar.SetExpPointsText(playerStats.expPoints);
            }
            }

        }
    }
}