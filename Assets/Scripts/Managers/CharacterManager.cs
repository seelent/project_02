﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace seelent
{
    public class CharacterManager : MonoBehaviour
    {
        [Header("Lock on Transform")]
        public Transform lockOnTransform;

        [Header("Combat Flags")]
        public bool isBlocking;
        public bool canBeRiposted;
        public bool canBeParried;
        public bool isParrying;
        
        [Header("Combat Colliders")]
        //public BoxCollider backStabBoxCollider;
        public BackStabCollider backStabCollider;
        public BackStabCollider riposteCollider;

        public int pendingCriticalDamage;
    }
}