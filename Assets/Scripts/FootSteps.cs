﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootSteps : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] clips;
    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Step(AnimationEvent animationEvent)
    {
       if(animationEvent.animatorClipInfo.weight > 0.1)
       {
        AudioClip clip = GetRandomClip();
        audioSource.PlayOneShot(clip);
        }
    }

    public void StepRun(AnimationEvent animationEvent)
    {
       if(animationEvent.animatorClipInfo.weight > 0.1)
       {
        AudioClip clip = GetRandomClip();
        audioSource.PlayOneShot(clip);
        }
    }

    private AudioClip GetRandomClip()
    {
        return clips[UnityEngine.Random.Range(0, clips.Length)];
    }
}
